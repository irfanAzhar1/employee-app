package com.example.demo_binar.controller;

import com.example.demo_binar.entity.Division;
import com.example.demo_binar.service.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/division")
public class DivisionController {
    @Autowired
    private DivisionService divisionService;

    @GetMapping("/")
    public Iterable<Division> getAllDivision(){
        return divisionService.findAllDivision();
    }

    @GetMapping("/{id}")
    public Optional<Division> getDivisionById(@PathVariable("id") Long id){
        return divisionService.findDivisionById(id);
    }

    @PostMapping("/")
    public Division addDivision(@RequestBody Division division){
        return divisionService.saveDivision(division);
    }

    @PutMapping("/{id}")
    public Division updateDivision(@PathVariable("id") Long id, @RequestBody Division division){
        return divisionService.updateDivision(id, division);
    }

    @DeleteMapping("/{id}")
    public String deleteDivision(@PathVariable("id") Long id){
        return divisionService.deleteDivision(id);
    }
}

