package com.example.demo_binar.repository;

import com.example.demo_binar.entity.Division;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DivisionRepository extends JpaRepository<Division, Long> {
}