package com.example.demo_binar.service;

import com.example.demo_binar.entity.Division;
import com.example.demo_binar.repository.DivisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DivisionService {
    @Autowired
    private DivisionRepository divisionRepository;

    public Division saveDivision(Division division){
        return divisionRepository.save(division);
    }

    public Iterable<Division> findAllDivision(){
        return divisionRepository.findAll();
    }

    public Optional<Division> findDivisionById(Long id){
        return divisionRepository.findById(id);
    }

    public Division updateDivision(Long id, Division division){
        Optional<Division> divisionOptional = divisionRepository.findById(id);
        if (divisionOptional.isEmpty()){
            throw new IllegalArgumentException("Division with id " + id + " not found");
        }
        division.setId(id);
        return divisionRepository.save(division);
    }

    public String deleteDivision(Long id){
        Optional<Division> divisionOptional = divisionRepository.findById(id);
        if (divisionOptional.isEmpty()){
            throw new IllegalArgumentException("Division with id " + id + " not found");
        }
        divisionRepository.deleteById(id);
        return "Division with id " + id + " deleted";
    }
}
