package com.example.demo_binar.service;

import com.example.demo_binar.entity.Employee;
import com.example.demo_binar.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee saveEmployee(Employee employee){
        return employeeRepository.save(employee);
    }

    public Iterable<Employee> findAllEmployee(){
        return employeeRepository.findAll();
    }

    public Optional<Employee> findEmployeeById(Long id){
        return employeeRepository.findById(id);
    }

    public Employee updateEmployee(Long id, Employee employee){
        Optional<Employee> employeeUpdate = employeeRepository.findById(id);
        if(employeeUpdate.isEmpty()){
            throw new IllegalArgumentException("Employee not found");
        }
        employee.setId(id);
        return employeeRepository.save(employee);
    }

    public String deleteEmployee(Long id){
        Optional<Employee> employeeDelete = employeeRepository.findById(id);
        if(employeeDelete.isEmpty()){
            throw new IllegalArgumentException("Employee not found");
        }
        employeeRepository.deleteById(id);
        return "Employee deleted";
    }


}
