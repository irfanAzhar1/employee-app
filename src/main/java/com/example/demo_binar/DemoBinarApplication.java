package com.example.demo_binar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBinarApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBinarApplication.class, args);
	}

}
